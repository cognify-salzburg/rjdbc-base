# base image
FROM cognify/rjdbc-base-python-packages

MAINTAINER Norbert Walchhofer <nw@cognify.ai>

CMD ["/usr/bin/R"]
